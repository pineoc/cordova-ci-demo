# Cordova CI Demo

Cordova project, GitLab CI testing

[![pipeline status](https://gitlab.com/pineoc/cordova-ci-demo/badges/master/pipeline.svg)](https://gitlab.com/pineoc/cordova-ci-demo/commits/master)

## Env

- Base OS: Ubuntu 16.04, [docker image](https://hub.docker.com/r/beevelop/cordova/)
- Node.js 8.9.4
- Cordova 8.0.0
- Cordova Android 7.0.0
